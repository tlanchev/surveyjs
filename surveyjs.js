$(document).ready(function() {
//Initialising Firebase. DO NOT TOUCH
// Initialize Firebase
var config = {
  apiKey: "AIzaSyCn7ROkhDA1yieJriozjKPD5pSQjl6TsKc",
  authDomain: "course4-power-and-conflict.firebaseapp.com",
  databaseURL: "https://course4-power-and-conflict.firebaseio.com",
  projectId: "course4-power-and-conflict",
  storageBucket: "",
  messagingSenderId: "264169627784"
};
firebase.initializeApp(config);

//Arrays for Maths. DO NOT TOUCH
var titleArray = new Array();
var valuesArray = new Array();
var receivedArrayTotal = 0;
var totalEntries = 0;
var theTotal = 0;  // Variable that holds the Total. DO NOT TOUCH
var chart = [];

//Initiates Survey. DO NOT TOUCH
Survey.Survey.cssType = "bootstrap";

var surveyJSON = {pages:[{name:"page1",elements:[{type:"rating",name:"How confident are you conducting market analysis?",description:"5 - most confident",isRequired:true},{type:"rating",name:"How confident are you in evaluating the feasibility of a new opportunity?",title:"How confident are you in evaluating the feasibility of a new opportunity?",description:"5 - most confident",isRequired:true},{type:"rating",name:"How confident are you in discovering new ways to improve existing products",title:"How confident are you in discovering new ways to improve existing products",description:"5 - most confident",isRequired:true}]},{name:"page2",elements:[{type:"rating",name:"How confident are you in discovering new ways to improve existing products?",title:"How confident are you in discovering new ways to improve existing products?",description:"5 - most confident"},{type:"rating",name:"How confident are you in creating new products and services and/or developing new ideas?",title:"How confident are you in creating new products and services and/or developing new ideas?",description:"5 - most confident"},{type:"rating",name:"How confident are you evaluating the feasibility of a new opportunity?",title:"How confident are you in evaluating the feasibility of a new opportunity?",description:"5 - most confident"}]},{name:"page3",elements:[{type:"rating",name:"How confident are you in targeting new markets and customers?",title:"How confident are you in targeting new markets and customers?",description:"5 - most confident",isRequired:true},{type:"rating",name:"How confident are you in participating in global markets?",title:"How confident are you in participating in global markets?",description:"5 - most confident",isRequired:true},{type:"rating",name:"How confident are you in calculating and accept risks?",title:"How confident are you in calculating and accept risks?",description:"5 - most confident",isRequired:true},{type:"rating",name:"How confident are you in reducing risk and uncertainty?",title:"How confident are you in reducing risk and uncertainty?",isRequired:true},{type:"rating",name:"How confident are you in working productively under continuous pressure?",title:"How confident are you in working productively under continuous pressure?",isRequired:true},{type:"rating",name:"How confident are you in making decisions under uncertainty?",title:"How confident are you in making decisions under uncertainty?",isRequired:true}]}]}

console.log(surveyJSON)
// Create div for each page to show this page map
var surveyPages = surveyJSON.pages ;
var pagesNum = Object.keys(surveyPages).length; // Get number of pages
var mapDiv = $("#container0")[0].outerHTML;
for (var i = 1; i < pagesNum; i++) {
  $("#maps").append(mapDiv);
  $("#maps div:last-child").attr("id" , 'container'+i)
}
var maps = new Array();
for (var key in surveyPages) {
    if (surveyPages.hasOwnProperty(key)) {
        var pageObj = surveyPages[key] ;
        maps[key] = new Array();
        maps[key]['results'] = new Array();
        for (var pageKey in pageObj) {
          if (pageObj.hasOwnProperty(pageKey)) {
            maps[key][pageKey] = pageObj[pageKey];
          }
        }
    }
}
console.log(maps)

//What happens, when survey completed. DO NOT TOUCH
var survey = new Survey.Model(surveyJSON);
$("#surveyContainer").Survey({
    model: survey,
    onComplete: doMaths
});

//Maths and Chart function. DO NOT TOUCH
function doMaths (survey) {

  document.getElementById('surveyContainer').style.display = "none";

  survey.sendResult('faeb5984-f51b-4a58-a917-509e536739ed');

  for (i=0; i < Object.keys(survey.data).length; i++) {
    titleArray.push(Object.keys(survey.data)[i]);
  }

  for (i=0; i < Object.values(survey.data).length; i++) {
    valuesArray.push(Object.values(survey.data)[i]);
  }

  for(var i = 0, len = valuesArray.length; i < len; i++) {
    theTotal += valuesArray[i];  // Iterating over the variable
  }

  console.log(titleArray)

  makeChart()
  giveFeedback()
  uploadData()
  getData()
}

function makeChart() {

    for (var i = 0; i < maps.length; i++) {
      var elements = maps[i]['elements'] ;
      for (var j = 0; j < elements.length; j++) {
        var element = titleArray.indexOf(elements[j].name) ;
        if (element!==-1) {
          maps[i]['results'].push(valuesArray[element]);
        }
      }
    }
    console.log(maps);

    for (var i = 0; i < maps.length; i++) {
      var elements = maps[i]['elements'] ;
      var results = maps[i]['results'] ;
      var pageTitleArray = new Array();
      var pageValuesArray = new Array();
      for (var j = 0; j < elements.length; j++) {
        pageTitleArray.push(elements[j].name);
      }
      for (var n = 0; n < results.length; n++) {
        pageValuesArray.push(results[n]);
      }
      pageNum = i+1 ;
      Highcharts.chart('container'+i, {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Your confidence levels - Page ' + pageNum
        },
        xAxis: {
          categories: pageTitleArray,
        },
        yAxis: {
            title: {
                text: 'Your confidence levels - Page ' + pageNum ,
                align: 'high'
            }
        },
        series: [{
            data: pageValuesArray
        }]
      });

    }
}

function giveFeedback() {
  if (theTotal <= 11) {
    document.getElementById("feedback").innerHTML = "<h2>" + "YOU FAILURE!" + "</h2>";
  }

  else if (theTotal <= 23) {
    document.getElementById("feedback").innerHTML = "<h2>" + "YOU'RE NOT A MASSIVE FAILURE!" + "</h2>";
  }

  else if (theTotal >= 24) {
    document.getElementById("feedback").innerHTML = "<h2>" + "YOU ARE A SUCCESS" + "</h2>";
  }
}

function uploadData() {
  //create firebase database reference
  var dbRef = firebase.database();
  var contactsRef = dbRef.ref('results');

  firebase.database().ref('results').push({
    totalScore: theTotal
  });
}

function getData() {
  var dbRef = firebase.database();
  var leadsRef = dbRef.ref('results');
  leadsRef.on('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      receivedArrayTotal += parseInt(Object.values(childSnapshot.val()), 10);
      totalEntries++
    });
    dataManipulation()
  });
}

function dataManipulation() {
  var overallAverageScore = receivedArrayTotal / totalEntries;
  var personalAverageScore = receivedArrayTotal / theTotal;

  var feedbackContent = document.createElement("feedbackContent");
  feedbackContent.innerHTML = "The total for everyone on the course is: " + receivedArrayTotal.toFixed(0);
  document.getElementById('feedbackOverall').appendChild(feedbackContent);

  var overallAverage = document.createElement("overallAverage");
  overallAverage.innerHTML = "<br>" + "The average for everyone on the course is: " + overallAverageScore.toFixed(0);
  document.getElementById('feedbackOverall').appendChild(overallAverage);

  var personalAverage = document.createElement("personalAverage");
  personalAverage.innerHTML = "<br>" + "Your personal average: " + personalAverageScore.toFixed(0);
  document.getElementById('feedbackOverall').appendChild(personalAverage);

  var personalScore = document.createElement("personalScore");
  personalScore.innerHTML = "<br>" + "Your score is: " + theTotal;
  document.getElementById('feedbackOverall').appendChild(personalScore);
}

}); //Closes jQuery. DO NOT TOUCH
