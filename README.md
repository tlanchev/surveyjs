# SurveyJS charts

## Tech stack
* [SurveyJS](https://surveyjs.io/)
* [Bootstrap](https://getbootstrap.com/)
* [Jquery](https://jquery.com/)
* [Firebase](https://console.firebase.google.com/u/0/)

##How to add a new Firebase Database to your survey
###You need to do this every time you create a new survey
1. Create a Firebase account
2. Once that's done, login into your Firebase console [here](https://console.firebase.google.com/u/0/)
3. _Name_ your project anyway you like, choose any _country/region_ and click **Create Project**
4. Wait for a bit for the project to create
5. Once you are in the Console _of your project_ click "_Add Firebase to your web app_"
6. **Do not** copy the dependency. Just copy everything in between the `<script>` and `</script>` tags. Do not include the script tags
7. Paste everything between **line 3** and **line 11**
8. Once that's done, return to your browsers Firebase Console and click on the _Database_ menu option on the lefthand side
9. Click **Get started** on the _Realtime Database_ option
10. Then click on the _Rules_ tab
11. Copy and paste the following code:

      `{
        "rules": {
          ".read": true,
          ".write": true
        }
      }`

12. Click **Publish**
13. Don't worry about the security warning
14. Check if it works. If it works - you should see personalised feedback for everyone on the course

##How to add a new SurveyJS survey
1. Go to SurveyJS website and create a new survey
2. Use "rating" as a question format
3. !Important! Make sure that there is:
    * One question per page
    * A *title* and a *name* of the question are field in
4. Go to "embed survey" tab on SurveyJS
5. Make sure you use the jQuery, Bootstrap framework and show the survey on a page
6. Also tick the box "Load Survey JSON from server"
7. Only copy the line which starts with `var surveyJSON`
8. Paste instead of **line 25** in the surveyjs.js file
9. Enjoy

##BitBucket ReadMe
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
